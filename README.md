nodejs-ws-chat
==============

a simple nodejs websocket chat,
built with WS: https://github.com/einaros/ws
and jQuery.

watch demo here: http://web78.my.ibone.ch/nodeChat/

To run the chat server, import the ws module into your working directory:
`npm install ws`